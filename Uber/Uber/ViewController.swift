//
//  ViewController.swift
//  Uber
//
//  Created by ALEX VILATUÑA on 11/12/19.
//  Copyright © 2019 ALEX VILATUÑA. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    @IBOutlet weak var txtCorreo: UITextField!
    @IBOutlet weak var txtContraseña: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
         view.endEditing(true)
    }

    override func viewDidAppear(_ animated: Bool) {
        if let _ = Auth.auth().currentUser{
            performSegue(withIdentifier: "loginSuccessSegue", sender: self)
        }
    }
    @IBAction func loginButtonPressed(_ sender: Any) {
        guard let email = txtCorreo.text, let password = txtContraseña.text else {
                   return
               }
               Auth.auth().signIn(withEmail: email, password:password) { (result, error) in
              if error != nil {
               self.presetAlertWith(title: "Error", message: error?.localizedDescription ?? "ups error")
              } else {
                self.performSegue(withIdentifier: "loginSuccessSegue", sender: self)
                
                }
                   
               }
    }
    
    @IBAction func registreButtonPressed(_ sender: Any) {
    }
    
    private func presetAlertWith(title:String, message:String){
        let alertControler = UIAlertController(title:title, message: message, preferredStyle: .alert)
        let okalertAction=UIAlertAction(title:"ok", style: .default){_ in
            self.txtCorreo.text=""
            self.txtContraseña.text=""
            self.txtCorreo.becomeFirstResponder()
        }
        alertControler.addAction(okalertAction)
        present(alertControler, animated: true, completion: nil)
          
    }
    

}
