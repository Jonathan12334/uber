//
//  RideViewController.swift
//  Uber
//
//  Created by ALEX VILATUÑA on 12/17/19.
//  Copyright © 2019 ALEX VILATUÑA. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Firebase

class RideViewController: UIViewController, CLLocationManagerDelegate,MKMapViewDelegate{

    @IBOutlet weak var mapView: MKMapView!
let locationManager = CLLocationManager()
  let firestore = Firestore.firestore()
    
    var annotattion: [String:(MKPointAnnotation, UIColor)]=[:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        mapView.showsUserLocation = true
        mapView.delegate = self
        
        
        centerMapIn(location: locationManager.location)
        
        getAllUsers()
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
  let lastLocation = locations[locations.count - 1].coordinate
           let lat = lastLocation.latitude.description
           let long = lastLocation.longitude.description
    
           firestore
             .collection("location")
            .document(String(Auth.auth().currentUser!.uid))
             .setData(
               ["coordinates": [lat, long],
                "color": [63.0/255, 176.0/255, 66.0/255]
                ]
           )
        
    }
    
    
    func centerMapIn(location: CLLocation?){
        guard let location = location else{
            return
        }
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 2000, longitudinalMeters: 2000)
        
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    
    func getAllUsers(){
      firestore
        .collection("location")
        .getDocuments{(snapshot, error) in
            if error != nil{
                print(error)
                return
            }
            for document in snapshot!.documents{
                self.showUser(id: document.documentID)
            }
        }
        
        
        
    }
    func showUser(id: String){
        if id == String(Auth.auth().currentUser!.uid) {
            return
        }
        let annotation = UberAnotation()
        
         
            firestore
              .collection("location")
              .document(id)
              .getDocument { (snapshot, error) in
                if error != nil {
                  print(error)
                  return
                }
        
                let colorData = snapshot?.get("color") as! Array<Double>
                let rgbColor = colorData.map { CGFloat($0) }
                annotation.userColor = UIColor(red: rgbColor[0], green: rgbColor[1], blue: rgbColor[2], alpha: 1)
                annotation.userId = id
        
                self.mapView.addAnnotation(annotation)
            }
        
        
        firestore
        .collection("location")
        .document(id)
            .addSnapshotListener{ (snapshot, error) in
                if error != nil{
                    print(error)
                    return
                }
                
                
                
                let data = snapshot?.get("coordinates") as! Array<String>
                let coordinates = data.map{ Double($0)!}
                
                let lat = coordinates [0]
                let long = coordinates [1]
                
                let annotationCoordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                
                annotation.coordinate = annotationCoordinate
                
             
               // print(snapshot)
    }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
      if annotation is MKUserLocation{
        return nil;
      }else{
        let customAnnotation = annotation as! UberAnotation
        
        let pinIdent = "Pin";
        var pinView: UberAnnotationView;
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: pinIdent) as? UberAnnotationView {
          dequeuedView.annotation = customAnnotation;
          pinView = dequeuedView;
        
            
        }else{
            pinView = UberAnnotationView(annotation: customAnnotation, reuseIdentifier: pinIdent, image: nil, color: customAnnotation.userColor);
        
        }
        //pinView.pinTintColor = customAnnotation.userColor
        return pinView;
      }
    }
    
    
    @IBAction func centerlocationPressed(_ sender: Any) {
        centerMapIn(location: locationManager.location!)
    }
    
}
