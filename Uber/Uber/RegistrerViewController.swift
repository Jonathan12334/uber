//
//  RegistrerViewController.swift
//  Uber
//
//  Created by ALEX VILATUÑA on 11/19/19.
//  Copyright © 2019 ALEX VILATUÑA. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseStorage
import FirebaseAuth

class RegistrerViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, UIPickerViewDelegate {

    //MARK: -Attributes
    @IBOutlet weak var firstnameLabel: UITextField!
    @IBOutlet weak var lastnameLabel: UITextField!
    @IBOutlet weak var emailLabel: UITextField!
    @IBOutlet weak var birthdateLabel: UITextField!
    @IBOutlet weak var phoneLabel: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var pictureImageView: UIImageView!
    let bithDatePicker = UIDatePicker()
    @IBOutlet weak var activityview: UIActivityIndicatorView!
    
    var imagePicker = UIImagePickerController()
     //MARK: -Attributes
    override func viewDidLoad() {
        super.viewDidLoad()
        birthdateLabel.delegate = self
        bithDatePicker.datePickerMode = .date
        bithDatePicker.addTarget(self, action: #selector(dateValueChanged(_:)), for: .valueChanged)
        activityview.hidesWhenStopped = true
        activityview.isHidden = true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func pictureButtonPressed(_ sender: Any) {
        let actionSheet = UIAlertController(title: "add Picture", message: nil, preferredStyle: .actionSheet)
        let cameraAction=UIAlertAction(title: "camera", style: .default){(_) in
            
        }
        let libraryAction = UIAlertAction(title: "library", style: .default){(_) in
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum
                self.imagePicker.allowsEditing = true
                
                self.present(self.imagePicker, animated: true, completion: nil)
                
            }
        }
        
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(libraryAction)
        present(actionSheet, animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        pictureImageView.image = info[.editedImage] as! UIImage
       picker.dismiss(animated: true, completion: nil)
    }
    

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == birthdateLabel.tag {
            textField.inputView = bithDatePicker
        }

     }
  
   @objc func dateValueChanged(_ sender: UIDatePicker) {
        let format = DateFormatter()
        format.dateFormat = "DD - MM - YYYY"
        birthdateLabel.text = format.string(from: bithDatePicker.date)
    }
    
    
    @IBAction func saveButtonapressed(_ sender: Any) {
        activityview.isHidden = false
        activityview.startAnimating()
        createUser { userUID in
             self.uploadImage(userUID: userUID) { imageURL in
               self.saveToFirestore(userUID: userUID, imageURL: imageURL) {
                self.activityview.stopAnimating()
                self.performSegue(withIdentifier: "registrerSuccessSegue", sender: self)
               }
             }
           }
        
    }
        
    
    func uploadImage (userUID: String, completionHandler: @escaping (String)-> ()){
        
     let storage = Storage.storage()
        
        let ref = storage.reference()
        let profileImages = ref.child("users")
        
        let userId = UUID().uuidString
        let userPicture = profileImages.child("\(userId).jpg")
        
        let data = pictureImageView.image?.jpegData(compressionQuality: 0.5)!
        
        let _ = userPicture.putData(data!, metadata: nil) { (metadata, error) in
            
            if error != nil {
              self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
            }
            
            guard let _ = metadata else {
                return
            }
            userPicture.downloadURL { (url, error) in
                if error != nil {
                  self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
                }
            guard let downladURL = url else {
                return
            }
                completionHandler(downladURL.absoluteString)
                //self.saveToFirestore(imageURL: downladURL.absoluteString)
            }
        }
    }
    
    
    func saveToFirestore(userUID: String, imageURL: String, completionHandler: @escaping ()->()){
       let firestore = Firestore.firestore()
    firestore.collection("users").document(userUID).setData(
    [
                   "name": self.firstnameLabel.text ?? "",
                   "lastName": self.lastnameLabel.text ?? "",
                   "email": self.emailLabel.text ?? "",
                   "birthdate": self.birthdateLabel.text ?? "",
                   "password": self.passwordTxt.text ?? "",
                   "phone": self.phoneLabel.text ?? "",
                   "imageurl":"\(imageURL)"
               ]){ (error) in
                if error != nil {
                       self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
                     }
                if error == nil {
                    completionHandler()
                }
                   
               }
    }

    
    func createUser(completionHander: @escaping (String)-> ()){
        let auth = Auth.auth()
        auth.createUser(withEmail: emailLabel.text!, password: passwordTxt.text!){ ( authInfo, error) in
             if error != nil {
                self.showAlert(title: "Error", message: error?.localizedDescription ?? "Error")
                  }
            if error == nil {
                completionHander(authInfo!.user.uid)
            }
        }
    }
    
    
    func showAlert(title: String, message: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "ok", style: .default, handler: nil)
        
        alertController.addAction(okAlertAction)
        
        present(alertController, animated:true, completion: nil )
    }
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
